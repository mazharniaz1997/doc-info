import mysql.connector

class DbConnect:
    def get_connection(self):
        connection = mysql.connector.connect(
            host='localhost',
            database='doc-info',
            user='mazhar',
            password='Mazhar786$'
        )
        return connection
    
    def insert_links(self, _sp, _link):
        insert_query = """ INSERT INTO links (speciality, spc_link) VALUES (%s, %s) """
        val = (_sp, _link)
        connection = self.get_connection()
        cursor = connection.cursor(buffered=True)
        cursor.execute(insert_query, val)
        connection.commit()
        cursor.close()
        connection.close()
        return True

