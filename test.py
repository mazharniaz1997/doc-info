from PIL import Image, ImageFilter

image = Image.open("/home/mazharniaz/Projects/Personal/DocInfo/dinfo.jpg")
cropped_image = image.crop((11,164,185,178))
blurred_image = cropped_image.filter(ImageFilter.GaussianBlur(radius=5))
image.paste(blurred_image,(11,164,185,178))
image.show()