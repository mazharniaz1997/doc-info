from bs4 import BeautifulSoup
import requests
from db import DbConnect

con = DbConnect()

# function to extract html document from given url
def getHTMLdocument(url):
      
    # request for HTML document of given url
    response = requests.get(url)
      
    # response will be provided in JSON format
    return response.text

url_to_scrape = "https://www.marham.pk/doctors"

# create document
html_document = getHTMLdocument(url_to_scrape)
  
# create soap object
soup = BeautifulSoup(html_document, 'html.parser')

# main functionality
div = soup.find_all('div', {"class": "widget-list-item searchable scroll_to_item"})

for d in div:
    a = d.find('a', href=True)

    speciality = a['data-value']
    link = a['href']

    # insert doc speciality link
    con.insert_links(speciality, link)